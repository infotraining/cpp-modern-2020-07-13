#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

class IGadget
{
public:
    virtual void use() const = 0;
    virtual ~IGadget() = default; // user-declared
};

class Gadget : public IGadget
{
    int id_ {-1};
    std::string name_ = "unknown";

public:
    Gadget() = default;

    Gadget(int id)
        : Gadget{id, "not-set"} // delegating to other constructor
    {        
        name_.append("!!!");
    }

    Gadget(int id, const std::string& name)
        : id_ {id}
        , name_ {name}
    {
    }

    Gadget(const Gadget&) = delete;
    Gadget& operator=(const Gadget&) = delete;

    int id() const
    {
        return id_;
    }

    std::string name() const
    {
        return name_;
    }

    void use() const override
    {
        std::cout << "Using gadget: " << id() << " - " << name() << "\n";
    }
};

class SuperGadget : public Gadget
{
public:
    using Gadget::Gadget;

    SuperGadget(int id) : Gadget{id, "not-set(super gadget)"}
    {}

    void use() const override
    {
        std::cout << "Using super gadget: " << id() << " - " << name() << "\n";
    }
};

class HyperGadget final : public SuperGadget
{
public:
    using SuperGadget::SuperGadget;

    void use() const override
    {
        std::cout << "Using hyper gadget: " << id() << " - " << name() << "\n";
    }
};

TEST_CASE("inheritance of constructors")
{
    SuperGadget sg{1, "super-duper"};
    SuperGadget g2{4};

    Gadget& gref = sg;
    gref.use();
}

class NonCopyable
{
public:
    NonCopyable() = default;
    NonCopyable(const NonCopyable&) = delete;
    NonCopyable& operator=(const NonCopyable&) = delete;
};

TEST_CASE("Noncopyable")
{
    NonCopyable nc;
    //NonCopyable other = nc;
}

TEST_CASE("Gadget")
{
    Gadget g1(1, "ipad");
    g1.use();

    Gadget g2;
    g2.use();

    Gadget g3 {2};
    g3.use();

    // Gadget g4 = g3;  Gadget is non-copyable
    // g4.use();
}

namespace Templates
{
    template <typename T, typename = enable_if_t<std::is_integral_v<T>>>
    T calculate(T n)
    {
        return n;
    }

    // CZy template działa tak samo? Specjalizacja
    template <typename T, typename = void, typename = enable_if_t<std::is_floating_point_v<T>>>
    T calculate(T n)
    {
        return n;
    }
}

// namespace Cpp20
// {
//     template <std::integral T>
//     T calculate(T n)
//     {
//         return n;
//     }

//     // CZy template działa tak samo? Specjalizacja
//     template <std::floating_point T>
//     T calculate(T n)
//     {
//         return n;
//     }
// }

int calculate(int n)
{
    return n;
}

double calculate(double) = delete;

TEST_CASE("delete free function")
{
    REQUIRE(calculate(4) == 4);

    //REQUIRE(calculate(4.5) == 4.5);
}