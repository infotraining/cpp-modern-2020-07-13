#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

std::vector<int> create_data()
{
    return {1, 2, 3};
}

TEST_CASE("range-based for")
{
    SECTION("works with std containers")
    {
        vector<int> vec = {1, 2, 3};

        int sum = 0;
        
        for(int item : create_data())
        {
            sum += item;
        }

        REQUIRE(sum == 6);

        SECTION("is interpreted as")
        {
            auto&& container = create_data();
            for(auto it = container.begin(); it != container.end(); ++it)
            {
                int item = *it;
                sum += item;
            }
        }        
    }

    SECTION("works with native arrays")
    {
        long long tab[5] = {1, 2, 3, 4, 5};

        for(auto item : tab)
        {
            cout << item << " ";
        }
        cout << "\n";

        SECTION("is interpreted as")
        {
            auto&& container = tab;
            for(auto it = begin(container); it != end(container); ++it)
            {
                auto item = *it;
                cout << item << " ";
            }
            cout << "\n";
        }
    }

    SECTION("works with initializer lists")
    {
        auto lst = {1, 2, 3};

        long long sum = 0;
        
        for(auto item : lst)
        {
            sum += item;
        }

        REQUIRE(sum == 6);

        for(const auto& item : {"one"s, "two"s, "three"s})	//o! to jest niezle !
        {
            cout << item << "; ";
        }
        cout << "\n";

        SECTION("is interpreted as")
        {
            auto&& container = {"one"s, "two"s, "three"s};
            for(auto it = begin(container); it != end(container); ++it)
            {
                const auto& item = *it;
                cout << item << "; ";
            }
            cout << "\n";
        }

        cout << "\n";
    }
}

struct Gadget
{
    string name;

    const std::string& get_name() 
    {
        return name;
    }
};

Gadget create_gadget()
{
    return Gadget{"ipadfsdfsdfsdfsdfsdfsdf"};//return Gadget().name("ipdsdgsdgsds");
}

TEST_CASE("BEWARE of temporary objects")
{
    for(const auto& c : Gadget{"ipad"}.get_name())
    {
        cout << c << ";";        
    }
    cout << "\n";

    SECTION("is interpreted as")
    {
        auto&& container =  Gadget{"ipad"}.get_name(); 
        // Gadget{"ipad"} is destroyed
        for(auto it = begin(container); it != end(container); ++it)
        {
            const auto& c = *it;
            cout << c << ";";
        }
        cout << "\n";
    }
}