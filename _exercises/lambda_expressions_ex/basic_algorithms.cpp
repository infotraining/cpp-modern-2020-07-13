#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <vector>
#include <array>

using namespace std;

TEST_CASE("lambda exercise")
{
    using namespace Catch::Matchers;

    vector<int> data = {1, 6, 3, 5, 8, 9, 13, 12, 10, 45};

    auto is_even = [](int num) { return !(num & 1); };

    SECTION("count even numbers")
    {
        auto evens_count = std::count_if(begin(data), end(data), is_even);

        REQUIRE(evens_count == 4);
    }

    SECTION("copy evens to vector")
    {
        vector<int> evens;

        std::copy_if(begin(data), end(data), std::back_inserter(evens), is_even);

        REQUIRE_THAT(evens, Equals(vector<int>{6, 8, 12, 10}));

        SECTION("back inserter")
        {
            vector<int> vec;
            REQUIRE(vec.size() == 0);

            auto it = std::back_inserter(vec); // back_insert_iterator<vector<int>

            *it = 1; // vec.push_back(1)
            *it = 2; // vec.push_back(2)
        }
    }

    SECTION("create container with squares")
    {
        vector<int> squares = data;

        std::for_each(squares.begin(), squares.end(), [](int& num) { num = num * num; });

        REQUIRE_THAT(squares, Equals(vector<int> {1, 36, 9, 25, 64, 81, 169, 144, 100, 2025}));
    }

    SECTION("create container with squares - alternative take")
    {
        vector<int> squares(data.size());

        std::transform(begin(data), end(data), begin(squares), [](int num) { return num * num; });

        REQUIRE_THAT(squares, Equals(vector<int> {1, 36, 9, 25, 64, 81, 169, 144, 100, 2025}));
    }

    SECTION("remove from container items divisible by any number from a given array")
    {
        const array<int, 3> eliminators = {3, 5, 7};

        auto new_end = std::remove_if(begin(data), end(data), 
                                        [&eliminators](auto num){ 
                                                return std::any_of(begin(eliminators), end(eliminators), 
                                                                [num](int eliminator) { 
                                                                    return num % eliminator == 0; 
                                                                });
                                        });
        data.erase(new_end, data.end()); // removes items

        REQUIRE_THAT(data, Equals(vector<int>{1, 8, 13}));
    }

    SECTION("calculate average")
    {
        auto sum = 0.0;
                    
        std::for_each(begin(data),end(data),[&sum](int num){sum += num;});

        double avg{sum/data.size()};        

        REQUIRE(avg == ::Approx(11.2));

        SECTION("create two containers - 1st with numbers less or equal to average & 2nd with numbers greater than average")
        {
            vector<int> less_equal_than_avg;
            vector<int> greater_than_avg;

            std::partition_copy(begin(data), end(data), std::back_inserter(less_equal_than_avg), std::back_inserter(greater_than_avg),
                [avg](int num) { return num <= avg; });

            REQUIRE_THAT(less_equal_than_avg, Contains(vector<int>{1, 6, 3, 5, 8, 9, 10}));
            REQUIRE_THAT(greater_than_avg, Contains(vector<int>{13, 12, 45}));
        }
    }
}
