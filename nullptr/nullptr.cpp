#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <memory>

using namespace std;
using namespace Catch::Matchers;

void foo(int* ptr)
{
    if (ptr)
        cout << "foo(int*: " << *ptr << ")\n";
    else
        cout << "foo(int*: null pointer)\n";
}

void foo(int value)
{
    void foo(int* ptr);

    cout << "foo(long: " << value << ")\n";
}

void foo(nullptr_t)
{
    cout << "foo(nullptr_t)\n";
}

TEST_CASE("why NULL is not perfect")
{
    int x = 10;

    foo(&x);

    foo(123);

    foo(NULL); // ERROR or foo(int) is called
}

TEST_CASE("nullptr is better NULL")
{
    int* ptr = nullptr;    

    foo(ptr);

    foo(123);

    foo(nullptr);
}

struct Gadget 
{
    int value;
};

TEST_CASE("nullptr - compare with pointers")
{
    Gadget* g = new Gadget{10};

    if (g)
    {
        cout << g->value << "\n";
    }	

    delete g;
}

TEST_CASE("nullptr has a type - nullptr_t")
{
    int* ptr = nullptr;
	int* ptr2 = NULL;
	
    foo(ptr);

    foo(nullptr);
}

TEST_CASE("unique_ptr")
{
    std::unique_ptr<Gadget> g = nullptr;

    REQUIRE(g.get() == nullptr);    
}