#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <map>

using namespace std;

namespace legacy_code
{
    class Container
    {
        int* dynamic_array_;
        size_t size_;
    public:
        Container(): dynamic_array_{new int[100]}, size_{100}
        {
            std::fill(begin(), end(), 0);
        }

        ~Container() 
        {
            delete [] dynamic_array_;
        }

        int* begin()
        {
            return dynamic_array_;
        }

        int* end()
        {
            return dynamic_array_  + size_;
        }

        const int* begin() const
        {
            return dynamic_array_;
        }

        const int* end() const
        {
            return dynamic_array_  + size_;
        }

        size_t size() const
        {
            return size_;
        }
    };
}

namespace explain
{
    template <typename T>
    auto begin(T& container)
    {
        return container.begin();
    }

    template <typename T, size_t N>
    T* begin(T (&tab)[N])
    {
        return tab;
    }   
    
    template <typename T>
    auto end(T& container)
    {
        return container.end();
    }

    template <typename T, size_t N>
    T* end(T (&tab)[N])
    {
        return tab + N;
    }
}

TEST_CASE("begin() + end() - explained")
{
    SECTION("vector")
    {
        std::vector<int> vec = {1, 2, 3};

        REQUIRE(*explain::begin(vec) == 1);
    }

    SECTION("native array")
    {        
        int tab[3] = {1, 2, 3};

        REQUIRE(*explain::begin(tab) == 1);
    }

    SECTION("custom container")
    {
        legacy_code::Container c;

        REQUIRE(*std::begin(c) == 0);
    }
}

TEST_CASE("auto")
{
	auto x(1);

    auto i = 42; // int   
    
    //auto a1{}; // ERROR - no expression
    SECTION("C++11/14")
    {
        int a0(0);
        auto a1(0); // int
        auto a2{0}; // std::initializer_list<int>
        //auto a3{ 1, 2, 3 }; // std::initializer_list<int>
        auto a4 = {1, 2, 3}; // std::initializer_list<int>
    }

    SECTION("C++17")
    {
        int a0(0);
        auto a1(0); // int
        auto a2{0}; // int
        //auto a3{ 1, 2, 3 }; // ERROR
        auto a4 = {1, 2, 3}; // std::initializer_list<int>
        auto a5 = {1}; // std::initializer_list<int>
    }

    SECTION("auto with modifiers: const, volatile, *")
    {
        constexpr auto x = 42;        
        const auto text = "text"; // const const char*        

        const int y = 665;
        const auto* cptr = &x;
        cptr = &y;
    }  

    SECTION("iteration over container")
    {
        int vec[5] = {1, 2, 3, 4, 5};

        for(auto it = cbegin(vec); it != cend(vec); ++it)
        {
            cout << *it << " ";
        }
        cout << "\n";

        SECTION("begin() + end() - C++11")
        {
            int vec[3] = {1, 2, 3};            

            for(auto it = begin(vec); it != end(vec); ++it)//only for static arrays
            {
                cout << *it << " ";
            }
            cout << "\n";            
        }
    }  
}

template <typename T>
void deduce1(T arg)
{    
    cout << __PRETTY_FUNCTION__ << "\n";
    // cout << __FUNCSIG__ << "\n"; // Visual C++
}

int foo(int) {}

TEST_CASE("auto type deduction - case 1")
{
    int x = 10;
    const int cx = 42;
    int& ref_x = x;
    const int& cref_x = x;
    int tab[10];

    auto ax1 = x; // int
    deduce1(x);

    auto ax2 = cx; // int
    deduce1(cx);

    auto ax3 = ref_x; // int
    deduce1(ref_x);

    auto ax4 = cref_x; // int
    deduce1(cref_x);

    auto ax5 = tab; // decay of array to a pointer: int*    
    auto text = "text"; // const char*

    const char* const cptrc = "abc"; // old known standard - const pointer to const value
    //cptrc = "efg";
    //cptrc[0] = 'A';

    auto const ax6 = cptrc;
    //ax6 = "ijk";
    //ax6 = 'I';

    auto f = foo; // int(*f)(int)
}

TEST_CASE("auto type deduction - case 2")
{
    int x = 10;
    const int cx = 42;
    int& ref_x = x;
    const int& cref_x = x;
    int tab[10];

    auto& ax1 = x; // int&
    auto& ax2 = cx; // const int&
    auto& ax3 = ref_x; // int&
    auto& ax4 = cref_x; // const int&
    auto& ax5 = tab; // int(&)[10]
    auto& ax6 = foo; // int(&)(int)

    auto& text = "text";
}

/////////////////////////////////
// auto as function return type

auto foo_cpp11() -> int // C++11
{
    return 4;
}

auto foo_cpp14() // C++14
{
    return 4;
}


//////////////////////////////////////////////////
// decltype


template<typename T>
auto multiply(const T& a, const T& b) -> decltype(a * b)
{
    return a * b;
}

struct Vector2D
{
    double x, y;
};

// scalar multiplication
double operator*(const Vector2D& a, const Vector2D& b)
{
    return a.x * b.x + a.y * b.y;
}

TEST_CASE("decltype")
{
    std::map<std::string, float> math_constants = { {"pi", 3.14}, {"e", 2.81 } };

    auto backup(math_constants); // copy of match_constants

    REQUIRE(backup.size() == 2);

    decltype(math_constants) other_constants; // object with the same type as math_constants

    REQUIRE(other_constants.size() == 0);    

    int x = 2;
    int y = 3;
    REQUIRE(multiply(x, y) == 6);

    Vector2D v1{1.0, 2.0};
    Vector2D v2{2.0, 3.0};
    REQUIRE(multiply(v1,v2) == Approx(8.0));
}

auto describe(int value)
{
    if (value % 2 == 0)
        return string("even");
    return "odd"s;
}

TEST_CASE("auto + multiple return")
{
    REQUIRE(describe(2) == "even");    
}

decltype(auto) get_value(std::map<std::string, float>& dict, const std::string& key)
{
    return dict.at(key);
}

TEST_CASE("get_value")
{
    std::map<std::string, float> math_constants = { {"pi", 3.14}, {"e", 2.72 } };

    get_value(math_constants, "pi") = 3.1415;

    REQUIRE(math_constants["pi"] == Approx(3.1415));
}

//////////////////////////////////////////////
// IsVoid - std::is_void
template <typename T>
struct IsVoid
{
    static constexpr bool value = false;
};

template <>
struct IsVoid<void>
{
    static constexpr bool value = true;
};

////////////////////////////////////////////
// IsPointer
template <typename T>
struct IsPointer
{
    static constexpr bool value = false;
};

template <typename T>
struct IsPointer<T*>
{
    static constexpr bool value = true;
};

TEST_CASE("decltype in static asserts")
{    
    auto x = 10;
    auto ptr = &x;

    static_assert(IsPointer<decltype(ptr)>::value, "Error");

    static_assert(std::is_same<int, decltype(x)>::value, "Error");
}

