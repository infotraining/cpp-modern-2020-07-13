#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <chrono>

using namespace std;
using namespace Catch::Matchers;

TEST_CASE("raw-string literals")
{
    string path1 = "c:\\nasz katalog\\backup";

    string path2 = R"(c:\nasz katalog\backup)";

    cout << path1 << "\n";    
    cout << path2 << "\n";

    REQUIRE(path1 == path2);

    SECTION("multiline")
    {
        string text1 = R"(Line1)"
R"(
Line2
Line3)";

        string text2 = R"(Line1
Line2
Line3)";

        REQUIRE(text1 == text2);

        cout << text1 << "\n";
    }
}

TEST_CASE("custom delimiters")
{
    const char* tmp = R">(cytaty: "text" po cytacie)>"; // a tak działa :)
    const char* text = R"motorola(cytaty: "(text)" po cytacie)motorola";

    cout << text << "\n";
    cout << tmp << "\n";
}

void foo(const char* txt)
{
    cout << "foo(c-string)\n";    
}

void foo(const std::string& txt)
{    
    cout << "foo(std::string)\n";
}

TEST_CASE("string literal - since C++14")
{
    using namespace std::literals;

    auto text1 = "text"; // const char*
    auto text2 = "text"s; // std::string
    auto text3 = L"text"s; // std::wstring
    auto S7 =  LR"("Hello \ world")"s; // std::wstring from a raw const wchar_t*

    foo("text");
    foo("text"s);

    //auto txt = "text"s;
    //ASSERT_NE(""s, txt); // w google test działa
}

TEST_CASE("other literals in std")
{
    auto timespan = 100ms;
    std::chrono::milliseconds timespan_ms(100);
}