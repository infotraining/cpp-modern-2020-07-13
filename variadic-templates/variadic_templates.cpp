#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <numeric>

using namespace std;
using namespace Catch::Matchers;

TEST_CASE("tuple")
{
    std::tuple<int, double, string> tpl{1, 3.14, "text"s};

    REQUIRE(std::get<0>(tpl) == 1);

    std::get<1>(tpl) = 2.71;
    REQUIRE(std::get<1>(tpl) == ::Approx(2.71));   

    auto other = std::make_tuple("pi"s, 3.14); //tuple<string, double>
}

std::tuple<int, int, double> calc_stats(const std::vector<int>& data)
{
    auto min = *min_element(begin(data), end(data));
    auto max = *max_element(begin(data), end(data));
    auto avg = std::accumulate(begin(data), end(data), 0.0) / data.size();

    return std::make_tuple(min, max, avg);
}

TEST_CASE("using tuple")
{
    vector<int> vec = { 42, 1, 665, 4};

    int min, max;
    double avg;
    
    std::tie(min, max, avg) = calc_stats(vec);

    // SECTION("structured bindings - since C++17")
    // {
    //     auto [minv, maxv, avgv] = calc_stats(vec);
    // }

    cout << "min: " << min << "\n";
    cout << "max: " << max << "\n";
}

///////////////////////////////
//  Variadic templates

void print()
{
    std::cout << "\n";
}

template <typename Head, typename... Tail>
void print(const Head& head, const Tail&... tail) // z co najmniej 1 elementem
{
    std::cout << head << " ";
    print(tail...); // recursive call with tail
}

namespace Cpp17
{
	/*
    // template <typename Head, typename... Tail>
    // void print(const Head& head, const Tail&... tail)
    // {
    //     std::cout << head << " ";
        
    //     if constexpr (sizeof...(tail) > 0)
    //     {
    //         print(tail...); // recursive call with tail
    //     }
    //     else
    //     {
    //         std::cout << "\n";
    //     }
    // }
	*/
}

TEST_CASE("variadic function")
{
    print(1, 3.14, "text"s, "abc", 1, 3.14, "text"s, "abc");
    print(1, 3.14);
}