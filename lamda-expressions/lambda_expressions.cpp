#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

class ClosureClass_4532845
{
public:
    auto operator()() const { cout << "hello from lambda!\n"; }
};

class GenericClosureClass_2378462783
{
public:
    template <typename T1, typename T2>
    auto operator()(const T1& a, const T2& b) const 
    { return a + b; }
};

TEST_CASE("lambda expression")
{
    auto l1 = []() { cout << "hello from lambda!\n"; };
    l1();

    SECTION("is interpreted as")
    {
        auto l1 = ClosureClass_4532845{};
        l1();
    }

    auto multiply = [](int a, int b) { return a * b; };

    REQUIRE(multiply(4, 2) == 8);
    REQUIRE(multiply(5, 3) == 15);

    SECTION("since C++14 - generic lambda expressions")
    {
        auto sum = [](const auto& a, const auto& b) { return a + b; };

        REQUIRE(sum(3, 4) == 7);
        REQUIRE(sum("abc"s, "def"s) == "abcdef"s);
    }
}

int foo()
{
    return 665;
}

struct Foo
{
    int operator()()
    {
        return 13;
    }
};

TEST_CASE("storing lambdas")
{
    SECTION("auto - preferred")
    {
        auto l = []{ return 42;}; // stack-only
        REQUIRE(l() == 42);
    }

    SECTION("std::function")
    {
        std::function<int()> f;
        
        SECTION("works function pointers")
        {
            f = &foo;
            REQUIRE(f() == 665);
        }

        SECTION("works with function objects")
        {
            f = Foo{};
            REQUIRE(f() == 13);
        }

        SECTION("works with lambda")
        {
            f = []() { return 42; }; // may be allocated using new
            REQUIRE(f() == 42);
        }
    }

    SECTION("function pointer - only non-capturing lambdas")
    {
        int (*fptr)();
        
        fptr = []() { return 42; };
        REQUIRE(fptr() == 42);
    }
}

TEST_CASE("lambda expression - return type signature")
{
    SECTION("C++11 - more complex lambda")
    {
        auto describe = [](int n) -> std::string {
            string result = "Number is ";
            if (n % 2 == 0)
                result += "even";
            else
                result += "odd";

            return result;
        };
    }

    SECTION("C++14")
    {
        auto describe = [](int n) -> string {
            cout << __PRETTY_FUNCTION__ << "\n";
            auto is_even = [](int n) { return n % 2 == 0; };
            if (is_even(n))
                return "even"s;
            else
                return "odd";            
        };

        describe(3);
    }
}

class ClosureClassByVal_23745276345
{
    const int factor_ = 2;
public:
    auto operator()(int x) const { return x * factor_; }
};

auto create_scaling_function(int factor)
{
    return [factor](int x) { return x * factor; };
}// tu factor idzie out of scope ale jego kopia jest w zwracanej "lambdzie"

TEST_CASE("capture & closer")	
{
    int factor = 2;

    SECTION("capture by value")
    {
        auto multiply = [factor](int x) { return x * factor; };	
        factor = 3;
        REQUIRE(multiply(4) == 8);

        auto scale_by_3 = create_scaling_function(3);
        REQUIRE(scale_by_3(4) == 12);
    }

    SECTION("capture by ref")
    {
        int counter = 0;

        auto call = [&counter]() { ++counter; };

        call();
        call();

        REQUIRE(counter == 2);
    }

    SECTION("init capture expression - C++14")
    {
        auto scaler = [mega_factor = factor * 10.5](int x) { return x * mega_factor; };        
    }

    SECTION("capture by ref")
    {
        int counter = 0;

        auto call = [&counter, factor]() { counter += factor; };
        auto call_alt1 = [=, &counter]() { counter += factor; };
        auto call_alt2 = [&, factor]() { counter += factor; };

        call();
        call();

        REQUIRE(counter == 4);
    }
}

static int global_counter = 665;

TEST_CASE("bug")
{
    // auto l = [global_counter] { return global_counter; };
    // ++global_counter;
    // REQUIRE(l() == 665);
}

class ClosureClassByValMutable_23745276345
{
    int counter_ = 2;
public:
    auto operator()() { return ++counter_; }
};

TEST_CASE("mutable")
{
    int counter = 0;

    auto call = [counter]() mutable { return ++counter; };
    REQUIRE(call() == 1);
    REQUIRE(call() == 2);
    REQUIRE(call() == 3);
}

class Gadget
{
    int id1;
    int id2;
public:
    Gadget(int id1, int id2) : id1{id1}, id2{id2}
    {}

    void use()
    {        
        auto print = [this] { cout << "Gadget(" << id1 << ", " << id2 << ")\n"; };

        print(); 
    }    

    void set_id(int id1, int id2)
    {
        this->id1 = id1;
        this->id2 = id2;
    }
};

TEST_CASE("lambda in class methods")
{
    Gadget g{42, 554};
    g.use();

    g.set_id(234, 2355);
    g.use();
}

namespace explain
{
    template <typename Iterator, typename Func>
    void for_each(Iterator first, Iterator last, Func f)
    {
        for(auto it = first; it != last; ++it)
            f(*it);
    }
}

TEST_CASE("passing lambdas to function")
{
    vector<int> vec = {1, 234, 66, 34545, 665, 2, 3};        

    explain::for_each(vec.begin(), vec.end(), [](const auto& x) { cout << x << " ";});
    cout << "\n";

    int threshold = 500;

    auto pos_gt_500 = std::find_if(vec.begin(), vec.end(), 
                                   [threshold](int x) { return x > threshold; });

    REQUIRE(*pos_gt_500 == 34545);             

    vector<string> words = { "Ala", "ma", "kokarde", "!"};

    auto compare_by_length = [](const auto& a, const auto& b) { return a.length() < b.length(); };

    std::sort(words.begin(), words.end(), compare_by_length);              

    for(const auto& word : words)
        cout << word << " ";
    cout << "\n";          
}