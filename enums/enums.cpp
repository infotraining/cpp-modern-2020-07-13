#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

enum Coffee : uint8_t;

void drink(Coffee c);

enum Coffee : uint8_t { 
    Espresso = 42, 
    Cappucino, 
    Late, 
    Chemex = std::numeric_limits<std::underlying_type_t<Coffee>>::max() 
};

void drink_coffee(uint8_t index)
{
    cout << "drink coffee: index=" << static_cast<unsigned int>(index) << "\n";
}

enum Poison { Cyanide };

TEST_CASE("enums")
{
    Coffee c = Espresso;
    
    REQUIRE(sizeof(c) == 1);

    std::underlying_type<Coffee>::type index1 = c;
    std::underlying_type_t<Coffee> index2 = c; // since C++14

    REQUIRE(index1 == 42);
    REQUIRE(Chemex == 255);

    SECTION("implicit conversion to integral type is a trap")
    {
    drink_coffee(Cappucino);
    drink_coffee(Cyanide); // BUG!!!
    }
}

enum class Day : uint8_t { Mon = 1, Tue, Wed, Thd, Fri, Sat, Sun };

TEST_CASE("scoped enums - new enums since C++11")
{
    Day day_of_week = Day::Mon;

    REQUIRE(day_of_week == Day::Mon);

    SECTION("implicit conversion to integral type is forbidden")
    {
        std::underlying_type_t<Day> index_of_day = static_cast<int>(Day::Thd);
        REQUIRE(index_of_day == 4);
    }

    SECTION("implicit conversion from integral type is forbidden")
    {
        int index = 6;
        Day day_of_week = static_cast<Day>(index);

        REQUIRE(day_of_week == Day::Sat);
    }
}