#include "catch.hpp"
#include <array>
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <list>

using namespace std;
using namespace Catch::Matchers;

struct Point
{
    int x, y;
};

struct Vector2D
{
    int64_t x, y;

    Vector2D(int64_t x, int64_t y)
        : x {x}
        , y {y}
    {
    }
};

TEST_CASE("uniform initialization")
{
    SECTION("C++98")
    {
        int x1(10);
        //int x2(); // most vexing parse
        Point pt {1, 2};
        Vector2D vec(1, 2);

        int tab[3] = {1, 2, 3};

        std::vector<int> container;
        container.push_back(1);
    }

    SECTION("C++11")
    {
        int x {10};
        int zero {}; // value initialization - set default zero value
        int* ptr {}; // int* ptr = nullptr;

        Point pt {1, 2};
        Vector2D vec {1, 2}; // init using constructor

        int tab[3] = {1, 2, 3};
        const vector<int> container = {1, 2, 3};

        uint64_t n = 64;
        uint64_t m {n};
    }
}

struct Data
{
    int a, b;
    double pi;
    std::string name;
    int coord[3];
    int z = {0};
    int sum() { return a + b; } // czy to nadal jest agregatem ?
};

struct ExtraString : std::string
{
    bool flag;
};

TEST_CASE("aggregate initialization")
{
    Data d1 {1, 2, 3.14}; // Data{1, 2, 3.14, ""s, [0, 0, 0]}
    Data d2 {1}; // Data{1, 0, 0.0, ""s, [0, 0, 0]}

    int tab[5] = {1, 2, 3}; // [1, 2, 3, 0, 0]

    std::array<int, 5> arr = {1, 2, 3};

    REQUIRE_THROWS(arr.at(10));
}

////////////////////////////////////////////
//

class DataSet
{
    int* array_;
    size_t size_;

public:
    using iterator = int*;
    using const_iterator = const int*;

    DataSet(size_t size)
        : array_ {new int[size]}
        , size_ {size}
    {
        std::fill(array_, array_ + size_, 0);
    }

    DataSet(std::initializer_list<int> lst)
        : array_{new int[lst.size()]}
        , size_{lst.size()}
    {        
        std::copy(lst.begin(), lst.end(), array_);
    }

    ~DataSet()
    {
        delete[] array_;
    }

    size_t size() const
    {
        return size_;
    }

    int& operator[](size_t index)
    {
        return array_[index];
    }

    const int& operator[](size_t index) const
    {
        return array_[index];
    }

    iterator begin()
    {
        return array_;
    }

    iterator end()
    {
        return array_ + size_;
    }

    const_iterator begin() const
    {
        return array_;
    }

    const_iterator end() const
    {
        return array_ + size_;
    }
};

TEST_CASE("Data")
{
    // DataSet dataset(10);
    // dataset[4] = 42;
    // dataset[8] = 665;

    DataSet dataset = {1, 2, 3, 4, 5};

    for(const auto& item : dataset)
    {
        cout << item << " ";
    }
    cout << "\n";

    SECTION("constructor with initializer_list is preferred when construction is {}")
    {
        DataSet ds1(10);
        DataSet ds2{10};

        REQUIRE(ds1.size() == 10);
        REQUIRE(ds2.size() == 1);

        std::vector<int> vec1(10, 1);
        std::vector<int> vec2{10, 1};

        REQUIRE(vec1.size() == 10);
        REQUIRE(vec2.size() == 2);
    }
}

TEST_CASE("uniform initialization & std::initializer_list")
{
    std::vector<int> vec1 = {1, 2, 3, 4};
    std::vector<int> vec2{1, 2, 3, 4};

    vec1.insert(vec.begin() + 2, {0, 7, 8}); // vector<int>::insert(iterator pos, std::initializer_list<int> lst)

    REQUIRE(vec1 == vec2);

    std::set<int> set1 = {1, 2, 3, 5};
    std::map<std::string, int> dict = { {"one", 1}, {"two", 2}, {"three", 3} };
    std::list<std::string> lst = { "one", "two", "three" };
}