#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

struct X
{
    int  value;

    X(int v, const string& name) :
        value{v}
    {
        cout << "X(" << value << ", " << name << ")\n";
    }

    void use()
    {}

    X()
    {
        static int gen_id = 0;
        value = ++gen_id;

        cout << "X(" << value << ")\n";
    }

    X(const X&) = delete;
    X(X&&) = delete;
    X& operator=(const X&) = delete;
    X& operator=(X&&) = delete;

    ~X()
    {
        cout << "~X(" << value << ")\n";
    }
};

namespace legacy_code
{
    X* get_x();

    X* get_x()
    {
        return new X(665, "less evil");
    }

    void use_x(X* ptr)
    {
        if (ptr)
            cout << "using X: " << ptr->value << endl;

        delete ptr;
    }

    void use(X* ptr)
    {
        if (ptr)
            cout << "using X: " << ptr->value << endl;
    }
}

namespace modern_cpp
{
    std::unique_ptr<X> get_x();

    std::unique_ptr<X> get_x()
    {
        return std::make_unique<X>(665, "less evil");
    }

    void use_x(std::unique_ptr<X> ptr)
    {
        if (ptr)
            cout << "using X: " << ptr->value << endl;
    } // automatic delete ptr

    void use(X* ptr)
    {
        if (ptr)
            cout << "using X: " << ptr->value << endl;
    }
}

class Owner
{
    std::unique_ptr<X> x_;
public:
    Owner(std::unique_ptr<X> x) : x_{std::move(x)}
    {}
};

TEST_CASE("clean & safe code")
{
    using namespace modern_cpp;

    get_x(); // no mem-leak

    std::unique_ptr<X> ptr = get_x();
    ptr->use();
    
    X stack{1, "on stack"};
    X* evil_ptr = &stack;
    //use_x(evil_ptr); // ERROR

    use(ptr.get());

    Owner o1{std::move(ptr)}; // o1 becomes an owner of ptr by explicit move (l-value)
    REQUIRE(ptr == nullptr);

    Owner o2{get_x()}; // o2 becomes an owner of ptr by implicit move (r-value)

    SECTION("custom dellacators")
    {
        auto file_closer = [](FILE* f) { fclose(f); };
        //std::unique_ptr<FILE, int(*)(FILE*)> file_txt{fopen("txt.dat", "w"), &fclose};
        std::unique_ptr<FILE, decltype(file_closer)> file_txt{fopen("txt.dat", "w"), file_closer};

        fprintf(file_txt.get(), "text");
    } // file_txt::~unique_ptr calls fclose
}

std::unique_ptr<X> uber_owner_ptr = modern_cpp::get_x();

TEST_CASE("using std::unique_ptr & raw-ptr")
{
    std::unique_ptr<X> owner_ptr = modern_cpp::get_x();

    X* user1 = owner_ptr.get();
    user1->use();

    uber_owner_ptr = std::move(owner_ptr);
}

struct User
{
    std::weak_ptr<X> x;

    void use()
    {
        std::shared_ptr<X> sx = x.lock();
        if (sx)
            sx->use();
        else
        {
            cout << "x is dead\n";
        }
    }
};

TEST_CASE("shared + weak pointer")
{
    std::shared_ptr<X> owner = modern_cpp::get_x();

    User u{owner};
    u.use();

    owner.reset();
    u.use();
}

TEST_CASE("legacy hell")
{
    using namespace legacy_code;

    {
        get_x();
        X* ptr = get_x();
        //...        
    } // mem-leak

    {
        use(get_x());
    } // mem-leak

    {
        X x{1, "evil"};
        X* ptr = &x;
        //use_x(ptr); // seg-fault     
    }    
}

