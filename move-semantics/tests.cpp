#include "catch.hpp"
#include <deque>
#include <iostream>
#include <map>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

string full_name(const string& first_name, const string& last_name)
{
    return first_name + " " + last_name;
}

TEST_CASE("reference binding")
{
    string name = "jan";

    SECTION("C++98")
    {
        string& cref_name = name; // lvalue ref can bind to lvalue

        const string& ref_full_name = full_name(name, "kowalski"); // lvalue ref to const can bind to rvalue
    }

    SECTION("C++11")
    {
        string&& rref_full_name = full_name(name, "kowalski");
        rref_full_name[0] = 'J';

        // string&& rref_name = name; // ERROR - rvalue ref can not bind to lvalue
    }
}

template <typename T>
class UniquePtr
{
    T* ptr_;

public:
    UniquePtr(nullptr_t)
        : ptr_ {nullptr}
    {
    }

    explicit UniquePtr(T* ptr = nullptr)
        : ptr_ {ptr}
    {
    }

    UniquePtr(const UniquePtr&) = delete;
    UniquePtr& operator=(const UniquePtr&) = delete;

    UniquePtr(UniquePtr&& other) noexcept // move constructor
        : ptr_ {other.ptr_}
    {
        other.ptr_ = nullptr;
    }

    UniquePtr& operator=(UniquePtr&& other) noexcept // move assignment
    {
        if (this != &other)
        {
            delete ptr_;
            ptr_ = other.ptr_;
            other.ptr_ = nullptr;
        }

        return *this;
    }

    explicit operator bool() const
    {
        return ptr_ != nullptr;
    }

    ~UniquePtr()
    {
        delete ptr_;
    }

    T* get() const
    {
        return ptr_;
    }

    T* operator->() const
    {
        return ptr_;
    }

    T& operator*() const
    {
        return ptr_;
    }
};

struct Gadget
{
    int value {};
    std::string name {};

    Gadget() = default;

    Gadget(int v)
        : value {v}
    {
        cout << "Gadget(" << value << ")\n";
    }

    Gadget(int v, std::string n)
        : value {v}
        , name {std::move(n)}
    {
    }

    void use() const
    {
        cout << "Using Gadget(" << value << ")\n";
    }

    ~Gadget()
    {
        cout << "~Gadget(" << value << ")\n";
    }
};

template <typename T, typename... Arg>
UniquePtr<T> make_UniquePtr(Arg&&... arg) // Arg&& - forwarding reference
{
    return UniquePtr<T>(new T(std::forward<Arg>(arg)...));
}

// template <typename T, typename Arg1, typename Arg2>
// UniquePtr<T> make_UniquePtr(Arg1 arg1, Arg2 arg2)
// {
//     return UniquePtr<T>(new T(arg1, arg2));
// }

void run(const std::string& arg)
{
}

void run(int n)
{
}

void log_msg(const std::string& msg)
{
    std::cout << "Log: " << msg << "\n";
}

template <typename Arg>
void run_wrapper(Arg&& arg)
{
    log_msg("run is called");
    run(std::forward<Arg>(arg)); // forwarding
}

TEST_CASE("move semantics in class")
{
    run_wrapper(1);
    run_wrapper("start"s);

    UniquePtr<Gadget> ptr1 = make_UniquePtr<Gadget>(1, "ipad");

    ptr1->use();

    UniquePtr<Gadget> ptr2 = std::move(ptr1); // move constructor

    ptr2->use();

    UniquePtr<Gadget> ptr3 = nullptr;

    ptr3 = std::move(ptr2); // move assignment

    ptr3->use();
}

std::vector<UniquePtr<Gadget>> create_gadgets()
{
    std::vector<UniquePtr<Gadget>> gadgets;

    gadgets.push_back(UniquePtr<Gadget>(new Gadget {2}));
    gadgets.push_back(UniquePtr<Gadget>(new Gadget {3}));

    UniquePtr<Gadget> g3 {new Gadget(3)};
    gadgets.push_back(std::move(g3));

    return gadgets;
}

TEST_CASE("UniquePtr & move semantics")
{
    vector<UniquePtr<Gadget>> items = create_gadgets();

    for (const auto& ptr : items)
    {
        if (ptr)
            ptr->use();
    }
}

namespace legacy_code
{
    struct Matrix
    {
        std::vector<int> rows;
    };

    Matrix* create_matrix()
    {
        return new Matrix {{1, 2, 3}};
    }

    void matrix_user()
    {
        Matrix* m = create_matrix();
    } // memory_leak
}

namespace modern_code
{
    struct Matrix
    {
        std::vector<int> rows;
    };

    Matrix create_matrix()
    {
        return Matrix {{1, 2, 3}};
    }

    void matrix_user()
    {
        Matrix m = create_matrix();
    }
}

// all 5 special functions are implicitly defaulted
struct X
{
    int value = 42;
    vector<int> vec = {1, 2, 3};

    void use() { }

    X() = default;
};

struct Y
{
    int* ptr;
};

TEST_CASE("implicit copy & move")
{
    X x; // default constructor
    REQUIRE(((x.value == 42) && (x.vec == std::vector<int> {1, 2, 3})));

    X x2 = x; // default copy
    REQUIRE(((x2.value == 42) && (x2.vec == std::vector<int> {1, 2, 3})));

    X x3 = std::move(x);
    REQUIRE(((x3.value == 42) && (x3.vec == std::vector<int> {1, 2, 3})));
    // x in after-move-state
    REQUIRE(((x.value == 42) && (x.vec.size() == 0)));

    Y y {new int(13)};
    Y y2 = std::move(y);
    REQUIRE(y.ptr == y2.ptr);

    REQUIRE(&y != &y2);
}

namespace legacy_code
{
    struct Person // copyable & non-moveable
    {
        std::string name;

        Person(const std::string& name = "not-set")
            : name(name)
        {
        }

        ~Person() { }
    };
}

namespace modern_cpp
{
    struct Person // copyable & moveable
    {
        std::string name;

        Person(const std::string& name = "not-set")
            : name(name)
        {
        }

        Person(const Person&) = default;
        Person& operator=(const Person&) = default;
        
        Person(Person&& other) noexcept(std::is_nothrow_constructible<Person>::value)
            : name{std::move(other.name)}
        {            
        }
        
        Person& operator=(Person&&) = default;

        ~Person()
        { /*log("")*/
        }
    };

    struct Copyable
    {
        int x;

        Copyable(const Copyable&) = default;
        Copyable& operator=(const Copyable&) = default;
    };
}

TEST_CASE("legacy code  + std::move")
{
    legacy_code::Person p {"jan"};

    legacy_code::Person p2 = std::move(p); // fallback to copy

    REQUIRE(p.name == "jan"s);
    //map.emplace(make_pair(string, new MyObj);
}

template <typename T>
class Queue
{
    std::deque<T> q_;

public:
    Queue() {}

	// void push(const T& item) // insert by copy
	// {
	// 	q_.push_front(item);
	// }

    // void push(T&& item) // insert by move
    // {
    //     q_.push_front(std::move(item));
    // }

    template <typename Item>
    void push(Item&& item) // Item&& - forwarding reference (universal reference)
    {
        static_assert(is_same<std::decay_t<Item>, T>::value, "Item must be same as T");
        q_.push_front(std::forward<Item>(item));
    }

    template <typename... Arg>
    void emplace(Arg&&... arg)
    {
        q_.emplace_front(std::forward<Arg>(arg)...); // q_.emplace(std::fwd<A1>(a1), std::fwd<A2>(a2));
    }

    void pop(T& item)
    {
        item = q_.back();
        q_.pop_back();
    }

    void empty() const
    {
        return q_.empty();
    }

    size_t size() const
    {
        return q_.size();
    }
};

TEST_CASE("Queue")
{
    Gadget g0{0, "zero"};
    
    Queue<Gadget> q;
    
    q.push(g0);
    q.push(Gadget {1, "one"});
    q.emplace(3, "three");
    q.push(Gadget {2});
    q.push(std::move(g0));

    {
        auto&& universal_refg = Gadget{5, "five"};
        q.push(std::forward<decltype(universal_refg)>(universal_refg)); // move
    }

    {
        Gadget g1{6, "six"};
        auto&& universal_refg = g1;
        q.push(std::forward<decltype(universal_refg)>(universal_refg)); // copy
    }
}

void may_throw()
{
    throw 42;
}

void evil() noexcept
{
    may_throw();
}