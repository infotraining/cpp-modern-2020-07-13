#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <array>

using namespace std;
using namespace Catch::Matchers;

constexpr int id = 665;
constexpr auto next_id = id + 1;

const int size_of_array = 64;
constexpr int big_size = 2 * size_of_array;

// int x = 54;
// constexpr auto value = x; // ERROR

constexpr int factorial(int n)
{
    return (n == 0) ? 1 : n * factorial(n-1);
}

TEST_CASE("constexpr")
{
    int tab[factorial(4)];
}

template <size_t N>
constexpr std::array<int, N> create_factorial_lookup()
{
    std::array<int, N> result = {};

    for(size_t n = 0; n < N; ++n)
        result[n] = factorial(n);

    return result;
}

TEST_CASE("constexpr functions")
{
    constexpr std::array<int, 5> factorial_lookup = create_factorial_lookup<5>();

    static_assert(factorial_lookup[0] == 1, "Error");
    static_assert(factorial_lookup[4] == 24, "Error");

    SECTION("runtime")
    {
        auto tab = create_factorial_lookup<6>();
        tab[3] = 23;
    }
}